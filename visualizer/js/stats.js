function Statistics(data){

    this.isNotExtraFunc = function(p){return !p.isExtra;};
    this.isExtraFunc = function(p){return p.isExtra;};

	this.items = [];
	this.books = {};
	this.config = {
		dateFormat: "LL", // https://momentjs.com/docs/#/parsing/string-format/
		bookMinimumReadingTime: 600, // Seconds. Books with total reading time of less than this are ignored
        dailyMinimumReadingTime: 180, // Seconds. Books with less reading time than this on a given day will be removed from this day.
		colors: "F06292,9575CD,64B5F6,4DD0E1,81C784,FFF176,FFB74D,FF8A65,A1887F,90A4AE".split(",").map(function(p){return "#" + p;}),
        readingPeriodLimit: 7
	};

	var ctrl = this;

	// Go through each book, and generate a complete list of all
    // reading dates and their information.
    data.forEach(function(book){

    	// Ignore this book if it's been read too short.
    	if(!book.isExtra && _.sumBy(book.dates, "time") < ctrl.config.bookMinimumReadingTime)
    		return;

    	book.cover = 'data/covers/' + book.cover;
        book.coverHtml = "<img src='" + book.cover + "' />";
        if(book.thumb){
            book.thumb = 'data/covers/' + book.thumb;
            book.thumbHtml = "<img src='" + book.thumb + "' />";
        }
        book.dates.forEach(function(date){
        	var item = {
                dateString: date.dateString,
                date: moment(date.dateString),
                time: date.time,
                words: date.words,
                filename: book.filename,
                source: book.source,
                device: book.device,
                author: book.author,
                title: book.title,
                cover: book.cover,
                thumb: book.thumb,
                isExtra: book.isExtra,
                id: book.id
            };
            item.jsDate = item.date.toDate();
            item.minutes = item.time / 60;
            item.hours = item.minutes / 60;
            item.days = item.hours / 24;

            if(item.isExtra || item.time >= ctrl.config.dailyMinimumReadingTime)
                ctrl.items.push(item);
        });

        if(ctrl.books[book.id]){
        	if(book.isExtra)
        		ctrl.books[book.id].readingPeriods.push(new ReadingPeriod(moment(_.first(book.dates).dateString), moment(_.last(book.dates).dateString)));
        }else{
        	book.readingPeriods = [];
        	if(book.isExtra)
        		book.readingPeriods.push(new ReadingPeriod(moment(_.first(book.dates).dateString), moment(_.last(book.dates).dateString)));
	        ctrl.books[book.id] = book;
	    }
        delete book.dates;
    });

    // Generate aggregated statistics for each book
    this._generateAggregatedStatistics();
	
	// Make some convenience variables
	this.sourceFiles = _(this.items).filter(this.isNotExtraFunc).map("source").uniq().value();
	this.bookFiles = Object.keys(this.books);
	this.bookList = Object.keys(this.books).map(function(p){ return ctrl.books[p]; });

    var dates = _(this.items).filter(this.isNotExtraFunc).map("date");
	this.minDate = dates.min();
	this.maxDate = dates.max();
    this.allDates = this._betweenDates(this.minDate, this.maxDate);
	this.hasExtras = _.some(this.items, "isExtra");

    // Add colors, making sure the same book receives the same color regardless of source.
    var i = 0;
    var ids = _(this.bookList).forEach(function(book){
        book.color = ctrl.config.colors[i++ % ctrl.config.colors.length];
        book.mostRecentDate = _(book.items).map("date").max();
        book.firstDate = _(book.items).map("date").min();
    });
}

Statistics.prototype._betweenDates = function(startDate, endDate) {
    var now = startDate.clone(), dates = [];

    while (now.isSameOrBefore(endDate)) {
        dates.push(now.clone());
        now.add(1, 'days');
    }

    return dates;
};

Statistics.prototype._generateAggregatedStatistics = function(){
	var ctrl = this;
	Object.keys(ctrl.books).forEach(function(id){
        var book = ctrl.books[id];
        
        book.items = _(ctrl.items)
        	.filter(function(p){ return p.id == book.id})
        	.sortBy("dateString")
        	.value();

        book.isExtra = _.every(book.items, function(p){ return p.isExtra; });

        var period = new ReadingPeriod();
        _(book.items)
            .filter(ctrl.isNotExtraFunc)
            .forEach(function(p){
                
                if(!period.from){
                    period.push(p);
                    book.readingPeriods.push(period);
                    return;
                }

                if(p.date.diff(period.to, "days") <= ctrl.config.readingPeriodLimit){
                    period.push(p);
                }else{
                    period = new ReadingPeriod().push(p);
                }

            });

        // If the book has both manual and automatic items, check if one of the automatic
        // reading periods is a direct succession of one of the manual reading periods
        if(_.some(book.readingPeriods, ctrl.isNotExtraFunc) && _.some(book.readingPeriods, ctrl.isExtraFunc)){

            // Go through all automatic periods
            _(book.readingPeriods).filter(ctrl.isNotExtraFunc).forEach(function(p){

                    // Compare this period to all manual periods
                    _.filter(book.readingPeriods, ctrl.isExtraFunc).forEach(function(extra){

                        // Check if they overlap
                        if(p.overlaps(extra) || p.isClose(extra, 1)){

                            // If they do, extend the manual period with data from the automatic period
                            extra.extend(p);

                            // Then remove this automatic period from the list
                            book.readingPeriods = _.filter(book.readingPeriods, function(period){ return period != p; });

                            // And break the loop
                            return false;
                        }
                    });
                });
        }

        // Generate aggregated time, word and day statistics for the book
        // in total, as well as for the different devices.
        book.stats = {
            time: _(book.items).sumBy("time"),
            words: _(book.items).sumBy("words"),
            days: _(book.items).filter(ctrl.isNotExtraFunc).map("dateString").uniq().value(),
            allDays: _(book.items).map("dateString").uniq().value(),
            devices: _(book.items)
                .groupBy("device")
                .map(function(p, key){ return { 
                    device: key, 
                    time: _(p).sumBy("time"),
                    words: _(p).sumBy("words"),
                    days: _(p).filter(ctrl.isNotExtraFunc).map("dateString").uniq().value(),
                    allDays: _(p).map("dateString").uniq().value()
                }; }).value()
        };
        book.stats.wordsPerMinute = book.stats.words / (book.stats.time / 60);
    });
}

Statistics.prototype.getReadingDates = function(includeManualBooks){
    return _(this.items)
    	.filter(function(p){ return includeManualBooks || !p.isExtra; })
        .map("dateString")
        .uniq()
        .sort()
        .map(function(p){ return moment(p); })
        .value();
}

Statistics.prototype.getReadingWeeks = function(includeManualBooks){
	return _(this.items)
    	.filter(function(p){ return includeManualBooks || !p.isExtra; })
        .map("dateString")
        .uniq()
        .sort()
        .map(function(p){ return getWeek(moment(p)); })
        .uniqBy(function(p){ return str(p); })
        .value();
}

Statistics.prototype.getEventsForCalendar = function(minDate){
	var ctrl = this;
	return _(this.items)
        .filter(function(p){ return p.date.isSameOrAfter(minDate); })
	    .groupBy(function(p){ return p.dateString + "_" + p.id })
	    .map(function(p){
	        return {
	            id: p[0].id,
	            items: p,
	            color: ctrl.books[p[0].id].color,
	            startDate: p[0].jsDate,
	            endDate: p[0].jsDate,
                from: p[0].date,
                to: p[0].date
	        }
	    }).value();
}
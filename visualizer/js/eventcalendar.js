function EventCalendar(container, events, config){

	this.config = {
		separateYears: true,
		separateMonths: true,
		onRange: function(){},
		onClick: function(){},
		onMouseOver: function(){},
		onMouseOut: function(){}
	};

	for(var key in config)
		this.config[key] = config[key];

	this.events = events;
	this.container = container;
	this.calculate();
	this.draw();

	var ctrl = this;
	$(window).mouseup(function (e) {
		ctrl.finishRange(ctrl);
	});
}

EventCalendar.prototype.betweenDates = function(startDate, endDate) {
    var now = startDate.clone(), dates = [];

    while (now.isSameOrBefore(endDate)) {
        dates.push(now.clone());
        now.add(1, 'days');
    }

    return dates;
};

EventCalendar.prototype.calculate = function(){
	var ctrl = this;
	var data = _(this.events)
		.map(function(p){
			return {
				event: p,
				dates: ctrl.betweenDates(p.from, p.to)
			}
		});

	this.dates = data
		.map("dates")
		.flatten()
		.value();

	this.data = data
		.map(function(p){
			return _.map(p.dates, function(c){
				return {
					event: p.event,
					date: c
				};
			});
		})
		.flatten()
		.groupBy(function(p){ return p.date.format("YYYY-MM-DD"); })
		.value();

	this.firstDate = _.min(this.dates);
	this.lastDate = _.max(this.dates);
	this.months = [];

	var curr = this.firstDate.clone().startOf("month");
	var endMonth = this.lastDate.clone().startOf("month");
	while(curr <= endMonth){
		this.months.push(curr.clone());
		curr.add(1, "month");
	}
}

EventCalendar.prototype.draw = function(){
	var ctrl = this;
	ctrl.container.selectAll("div").remove();
	var contents = ctrl.container.append("div").attr("class", "calendar-container");

	var years = {};
	_.forEach(ctrl.months, function(month){
		var y = month.format("YYYY");
		var titleFormat = null;
		if(!years[y]){

			if(ctrl.config.separateYears){
				if(Object.keys(years).length > 0)
					contents.append("div").attr("class", "calendar-year-separator");	
				contents.append("div").attr("class", "calendar-year-header").text(y);
			}else{
				titleFormat = "MMMM, YYYY";
			}
			years[y] = 1;
		}
		ctrl.drawMonth(contents, month, titleFormat);
	});
}

EventCalendar.prototype.getBoxShadow = function(d){

	var events = _.map(this.data[d.format("YYYY-MM-DD")], "event");
	if(!events) return "";
					
	var boxShadow = '';
	var weight = 19 / events.length;
	_.forEach(events, function(event, i){
		if(boxShadow)
			boxShadow += ",";
		
		boxShadow += 'inset 0 -' + (parseInt(i) + 1) * weight + 'px 0 0 ' + events[i].color;
	});

	return "box-shadow: " + boxShadow + ";";
}

EventCalendar.prototype.mouseDown = function(ctrl, date){
	ctrl.rangeStart = date;
	ctrl.rangeEnd = date;
	ctrl.refreshRangeVisualization(ctrl);
}

EventCalendar.prototype.mouseMove = function(ctrl, date){
	if(!ctrl.rangeStart) return;
	ctrl.rangeEnd = date;
	ctrl.refreshRangeVisualization(ctrl);
}

EventCalendar.prototype.mouseUp = function(ctrl, date){
	ctrl.rangeEnd = date;
	ctrl.finishRange(ctrl);
}

EventCalendar.prototype.finishRange = function(ctrl){

	if(!ctrl.rangeStart) return;

	var min = ctrl.rangeStart;
	var max = ctrl.rangeEnd;
	if(ctrl.rangeEnd.isBefore(ctrl.rangeStart)){
		min = ctrl.rangeEnd;
		max = ctrl.rangeStart;
	}

	var events = _(ctrl.betweenDates(min, max))
		.map(function(p){ return ctrl.data[p.format("YYYY-MM-DD")]; })
		.flatten()
		.filter(function(p){ return p; })
		.map(function(p){ return p.event; })
		.uniqBy(function(p){ return p.id; })
		.value();

	ctrl.config.onRange(min, max, events);

	ctrl.rangeStart = null;
	ctrl.rangeEnd = null;
	ctrl.refreshRangeVisualization(ctrl);
}

EventCalendar.prototype.refreshRangeVisualization = function(ctrl){
	var jq = $(ctrl.container.node())
	if(!ctrl.rangeStart){
		jq.find(".date-selected").removeClass("date-selected");
		return;
	}

	var min = ctrl.rangeStart;
	var max = ctrl.rangeEnd;
	if(ctrl.rangeEnd.isBefore(ctrl.rangeStart)){
		min = ctrl.rangeEnd;
		max = ctrl.rangeStart;
	}

	var selectedDates = _(ctrl.betweenDates(min, max))
		.map(function(p){ return p.format("YYYY-MM-DD"); })
		.keyBy(function(p){ return p; })
		.value();

	jq.find("[data-date]").each(function(){
		if(selectedDates[$(this).attr("data-date")])
			$(this).addClass("date-selected");
		else
			$(this).removeClass("date-selected");
	});
}

EventCalendar.prototype.getEvents = function(ctrl, date){
	var events = ctrl.data[date.format("YYYY-MM-DD")];
	if(!events) return [];
	return _.map(events, "event");
}

EventCalendar.prototype.drawMonth = function(container, month, titleFormat){
	
	if(this.config.separateMonths){
		container = container
			.append("div")
			.attr("class", "calendar-month");
	}

	container
		.append("div")
		.attr("class", "calendar-month-header" + (this.config.separateMonths ? "" : " calendar-month-header-compact"))
		.text(month.format(titleFormat || "MMMM"));

	var m = month.format("M");
	var firstDay = month.clone().startOf("month");
	var lastDay = month.clone().endOf("month");

	if(this.config.separateMonths)
		firstDay = firstDay.startOf("week");

	var days = this.betweenDates(firstDay, lastDay);

	var ctrl = this;
	container
		.selectAll("div.ym-" + month.format("YYYY-MM"))
		.data(days)
		.enter()
			.append("div")
			.attr("data-date", function(d){ return d.format("M") == m ? d.format("YYYY-MM-DD") : null })
			.attr("class", "calendar-day ym-" + month.format("YYYY-MM"))
			.attr("style", function(d){ return d.format("M") == m ? ctrl.getBoxShadow(d) : ""; })
			.on("mousedown", function(d) { ctrl.mouseDown(ctrl, d); })
			.on("mousemove", function(d) { ctrl.mouseMove(ctrl, d); })
			.on("mouseup", function(d) { ctrl.mouseUp(ctrl, d); })
			.on("mouseover", function(d){ ctrl.config.onMouseOver(this, d, ctrl.getEvents(ctrl, d)); })
			.on("mouseout", function(d){ ctrl.config.onMouseOut(this, d, ctrl.getEvents(ctrl, d)); })
			.on("click", function(d){ ctrl.config.onClick(this, d, ctrl.getEvents(ctrl, d)); })
			.append("div")
			.text(function(d){ return d.format("M") == m ? d.format("D") : ""; });
}
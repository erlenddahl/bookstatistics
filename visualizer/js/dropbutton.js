function DropButton(values, selectedIndex){
	this.values = values;
	this.selectedValue = values[selectedIndex];
}

DropButton.prototype.addTo = function(container, callback){

	var ctrl = this;

    var ig = container
        .append("div").attr("class", "input-group");
    
    var button = ig
        .append("button")
        .attr("class", "btn btn-outline-secondary dropdown-toggle")
        .attr("type", "button")
        .attr("data-toggle", "dropdown")
        .text(ctrl.selectedValue.title);

    ig.append("div")
        .attr("class", "dropdown-menu")
        .selectAll("a")
        .data(ctrl.values)
        .enter()
            .append("a")
            .attr("class", "dropdown-item")
            .attr("href", "")
            .text(function(d){ return d.title; })
            .on("click", function(d){
                d3.event.preventDefault();
                ctrl.selectedValue = d;
                button.text(d.title);
                callback(d);
            });

}
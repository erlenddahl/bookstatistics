function ReadingPeriod(from, to){
	this.from = from;
	this.to = to;
	this.items = [];
	this.isExtra = true;
}

ReadingPeriod.prototype.push = function(item){
	if(!this.from) this.from = item.date;
	this.items.push(item);
	this.to = item.date;
	this.isExtra = false;
	return this;
}

ReadingPeriod.prototype.extend = function(other){
	var old = this;
	_(other.items).forEach(function(item){
        old.items.push(item);
    });
    var dates = _.map(old.items, "date");
    dates.push(old.from);
    dates.push(old.to);
    old.from = _.min(dates);
    old.to = _.max(dates);
    old.isExtra = old.isExtra && other.isExtra;
}

ReadingPeriod.prototype.overlaps = function(other){
	return other.from.isSameOrBefore(this.to) && other.to.isSameOrAfter(this.from);
}

ReadingPeriod.prototype.isClose = function(other, limit){

	if(this.overlaps(other)) return true;

	if(this.from.isAfter(other.from))
		return this.from.diff(other.to, "days") <= limit;
	else
		return other.from.diff(this.to, "days") <= limit;
}

ReadingPeriod.prototype.toString = function(){
	return this.from.format("L") + " - " + this.to.format("L");
}

ReadingPeriod.prototype.dayCount = function(){
	return this.to.diff(this.from, "days") + 1;
}
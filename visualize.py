import os
import re
import csv
import json
from PIL import Image
import shutil
import sqlite3
import argparse
import tempfile
import datetime
import xml.etree.ElementTree
from parseBackup import BackupParser

class BackupFile:

    def __init__(self, filename):
        self.filename = filename
        parts = filename.replace(").mrpro", "").split("(")
        self.date = parts[0]
        self.device = parts[1]

class BackupVisualizer:

    def __init__(self, folder, library, extra, info, outputFolder, debug=False):
        self.folder = folder
        self.library = library
        self.extra = extra
        self.outputFolder = outputFolder
        self.debug = debug
        self.unknownId = 0

        if os.path.isfile(info):
            with open(info) as json_file:
                self.extraInfo = json.load(json_file)
        else:
            self.extraInfo = {}

    def getMergedJson(self):

        files = [f for f in os.listdir(self.folder) if os.path.isfile(os.path.join(self.folder, f))]
        files = [f for f in files if ".mrpro" in f]
        files = [BackupFile(f) for f in files]

        print("Found", len(files), "data files")

        newest = {}
        for file in files:
            if file.device not in newest:
                newest[file.device] = file
            elif file.date > newest[file.device].date:
                newest[file.device] = file

        print("Picked newest files for", len(newest), "devices:", ", ".join([k + ": " + newest[k].date for k in newest.keys()]))

        data = []
        for device in newest:
            bp = BackupParser(os.path.join(self.folder, newest[device].filename), self.debug)
            books = bp.parse()
            for book in books:
                book["device"] = device
                book["source"] = newest[device].filename
                data.append(book)

        print("Read", len(data), "reading entries.")

        return data

    def getXmlValue(self, lines, tagname):
        line = next(x for x in lines if tagname in x)
        start = line.find(">")
        end = line.rfind("<")
        return line[start+1:end]

    def slugify(self, value):
        """
        Converts to lowercase, removes non-alpha characters.
        """
        value = re.sub('[^\w\s-]', '', value).strip().lower()
        return value

    def hasAuthorAndTitle(self, book):
        if not "author" in book or not "title" in book:
            return False

        if book["author"] is None or book["title"] is None:
            return False

        return True

    def setId(self, book):

        if self.hasAuthorAndTitle(book):
            book["id"] = self.slugify(book["author"] + "---" + book["title"])
        else:
            book["id"] = "unknown_book_" + str(self.unknownId)
            self.unknownId += 1

        book["cover"] = self.slugify(book["id"]) + ".jpg"

    def setData(self, data):

        filenames = {}
        for book in data:
            fn = book["filename"]
            if fn not in filenames:
                filenames[fn] = { "authors": [], "titles": [] }
            if "author" in book and book["author"] != None and book["author"] != "":
                filenames[fn]["authors"].append(book["author"])
            if "title" in book and book["title"] != None and book["title"] != "":
                filenames[fn]["titles"].append(book["title"])

        if self.extraInfo is not None:

            for key in self.extraInfo:
                if key not in filenames:
                    filenames[key] = self.extraInfo[key]
                else:
                    for e in self.extraInfo[key]:
                        filenames[key][e] = self.extraInfo[key][e]

        print("Using filenames to group books")

        for book in data:
            if (not "author" in book or book["author"] is None or not "title" in book or book["title"] is None) and book["filename"] in filenames:
                fn = filenames[book["filename"]]
                if len(fn["authors"]) > 0:
                    book["author"] = fn["authors"][0]
                    print("    > Set author:", book["filename"])
                if "author" in fn:
                    book["author"] = fn["author"]
                    print("    > Set author:", book["filename"])
                if len(fn["titles"]) > 0:
                    book["title"] = fn["titles"][0]
                    print("    > Set title:", book["filename"])
                if "title" in fn:
                    book["title"] = fn["title"]
                    print("    > Set title:", book["filename"])

            self.setId(book)

    def readLibrary(self, data):

        print("Searching Calibre library for matching book file names ...")

        # If any books are missing author and title data, check if the Calibre 
        # library contains any book files with the same filenames. If it does,
        # read the metadata file, and set the author and title.
        for book in [x for x in data if not self.hasAuthorAndTitle(x)]:

            print("        > " + book["id"] + ", '" + book["filename"] + "'")

            for root, dirs, files in os.walk(self.library):
                for file in files:
                    if file == book["filename"]:
                        bookfile = os.path.join(root, file)

                        with open(os.path.join(root, "metadata.opf"), "r", encoding='utf-8') as f:
                            metadata = f.readlines()
                        
                        book["title"] = self.getXmlValue(metadata, "dc:title")
                        book["author"] = self.getXmlValue(metadata, "dc:creator")
                        self.setId(book)

                        print("            > Updated with author '" + book["author"] + "', title: '" + book["title"] + "', id: '" + book["id"] + "'.")

        calibreDbLocation = os.path.join(self.library, "metadata.db")

        if not os.path.isfile(calibreDbLocation):
            print("Failed to locate Calibre metadata database file (" + calibreDbLocation + ")")
            return

        print("Searching Calibre metadata database for meta data ...")
                    
        # Open the Calibre metadata database, and extract titles and authors.
        # Use this information to fill any holes, and locate the cover images for the books.
        with sqlite3.connect(calibreDbLocation) as conn:
            c = conn.cursor()

            meta = []
            for item in c.execute("SELECT books.title, books.sort, authors.name, books.author_sort, books.path, books.has_cover FROM books, books_authors_link, authors WHERE books.id = books_authors_link.book AND authors.id = books_authors_link.author"):
                meta.append(item)

            for book in data:
                title = book["title"].lower() if book["title"] is not None else None
                author = book["author"].lower() if book["author"] is not None else None

                for m in meta:

                    # Attempt to match this book with this meta data item using the book's filename and
                    # title/author (if any), and the meta data item's title/sort title and author/sort author.
                    useThis = False

                    if title is None or author is None:
                        # If a book is still missing title/author, check if the filename contains the title and author of 
                        # this meta data item. If so, we assume it's the correct book.
                        fn = book["filename"].lower()
                        useThis = (m[0].lower() in fn or m[1].lower() in fn) and (m[2].lower() in fn or m[3].lower() in fn)
                    else:                            
                        # Otherwise, simply check if the title/author matches this meta data item.
                        useThis = (title == m[0].lower() or title == m[1].lower()) and (author == m[2].lower() or author == m[3].lower())

                    # If there was no exact title/author match, but the book
                    # is still missing a cover, do one last attempt at matching
                    if not useThis and not self.hasCover(book):
                        if author == m[2].lower() or author == m[3].lower():
                            useThis = (m[0].lower() in title or m[1].lower() in title)

                    if not useThis:
                        continue
                    
                    print("    > Matched book '" + book["id"] + "' with Calibre book '" + m[4] + "'")

                    metaPath = os.path.join(self.library, m[4], "metadata.opf")

                    if book["title"] != m[1]:
                        print("        > '" + str(book["title"]) + "' => '" + m[1] + "'")
                        book["title"] = m[1]
                    if book["author"] != m[2]:
                        print("        > '" + str(book["author"]) + "' => '" + m[2] + "'")
                        book["author"] = m[2]

                    self.setId(book)

                    with open(metaPath, "r", encoding='utf-8') as f:
                        metadata = f.readlines()
                    
                    cover = os.path.join(self.library, m[4], next(x for x in metadata if "type=\"cover\"" in x).split("href=\"")[1].split("\"")[0])
                    self.copyCover(cover, book)

                    break

        print("Books still missing data:")
        missingData = [x for x in data if not self.hasAuthorAndTitle(x)]
        if len(missingData) < 1:
            print("    > No books.")
        for book in missingData:
            print("    > " + book["id"] + ", '" + book["filename"] + "'")

    def hasCover(self, book):
        return os.path.isfile(self.getCoverPath(book))

    def getCoverPath(self, book):
        return os.path.join(self.outputFolder, "data", "covers", book["cover"])

    def copyCover(self, source, book):

        destPath = self.getCoverPath(book)
        
        try:
            # Copy the full size cover image
            shutil.copyfile(source, destPath)
        except IOError as e:
            print(e)
            pass

        # Create a smaller version
        try:
            im = Image.open(source)
            im.thumbnail((60, 90), Image.ANTIALIAS)
            thumbPath = destPath.replace(".jpg", "_thumb.jpg")
            im.save(thumbPath, "JPEG")
            book["thumb"] = book["cover"].replace(".jpg", "_thumb.jpg")
        except IOError as e:
            print(e)
            pass

    def readExtra(self, data):

        count = 0
        with open(self.extra, "r", encoding="utf-8") as f:
            extraData = csv.reader(f, delimiter=';')
            for (i, row) in enumerate(extraData):
                if i < 1: continue

                item = {
                    "title": row[0],
                    "author": row[1],
                    "filename": "extra_" + str(i),
                    "source": "Extra CSV file",
                    "dates": [],
                    "isExtra": True
                }

                coverFile = os.path.join(os.path.dirname(self.extra), row[2])
                if os.path.isfile(coverFile):
                    item["cover"] = "extra_" + str(i) + "_" + row[2]
                    destFile = os.path.join(self.outputFolder, "data", "covers", item["cover"])
                    shutil.copyfile(coverFile, destFile)

                start = datetime.datetime.strptime(row[3], '%Y-%m-%d')
                end = datetime.datetime.strptime(row[4], '%Y-%m-%d')

                while start <= end:
                    item["dates"].append({
                        "dateString": start.strftime("%Y-%m-%d"),
                    })
                    start += datetime.timedelta(days=1)

                data.append(item)
                count += 1

        print("Read", count, "manual reading entries.")

    def copyrecursively(self, src, dest):
        if os.path.isdir(src):
            if not os.path.isdir(dest):
                os.makedirs(dest)
            files = os.listdir(src)
            for f in files:
                self.copyrecursively(os.path.join(src, f), os.path.join(dest, f))
        else:
            shutil.copyfile(src, dest)

    def visualize(self):

        data = self.getMergedJson()

        innerPath = os.path.join(self.outputFolder, "data", "covers")
        if not os.path.exists(innerPath):
            os.makedirs(innerPath)
            print("Created output directory")

        myPath = os.path.dirname(os.path.realpath(__file__))
        self.copyrecursively(os.path.join(myPath, "visualizer"), self.outputFolder)

        print("Copied static HTML template to output directory.")

        if self.extra is not None:
            self.readExtra(data)

        self.setData(data)

        if self.library is not None:
            self.readLibrary(data)

        outputFile = os.path.join(self.outputFolder, "data", "books.js")
        with open(outputFile, 'w') as outfile:
            outfile.write("var dataVersion='2019-05-06'; var data = new Statistics(" + json.dumps(data, indent=4) + ");")

        print("Wrote compiled statistics to output directory.")

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Unpacks the newest Moon+ backups for each device in the given folder, and generates a visualization.')
    parser.add_argument('-f', '--folder', required=True, action='store', type=str, help='The folder that contains the backup files.')
    parser.add_argument('-l', '--library', action='store', type=str, help='A folder containing a Calibre library, for metadata and cover image extraction.')
    parser.add_argument('-e', '--extra', action='store', type=str, help='A CSV file containing manual reading entries.')
    parser.add_argument('-i', '--info', action='store', type=str, help='A JSON file containing additional information about books.')
    parser.add_argument('-o', '--output', action='store', type=str, help='The destination directory for storing the visualization')
    parser.add_argument('-d', '--debug', action='store_true', help='Print debug information')

    args = parser.parse_args()

    bv = BackupVisualizer(args.folder, args.library, args.extra, args.info, args.output, args.debug)
    bv.visualize()
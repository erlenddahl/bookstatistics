import os
import json
import shutil
import random
import zipfile
import sqlite3
import argparse
import tempfile
import datetime

class BackupParser:

	def __init__(self, file, debug=False):
		self.file = file
		self.debug = debug

	def createTempDir(self):
		self.tempDir = os.path.join(tempfile.gettempdir(), "mbp-" + datetime.datetime.now().isoformat().replace(":", "-").replace(".", "-"))
		os.makedirs(self.tempDir)

		if(self.debug): print("Working directory: '" + self.tempDir + "'")

	def removeTempDir(self):
		shutil.rmtree(self.tempDir)

	def unzip(self):
		if(self.debug): print("Unzipping file '" + self.file + "'")
		with zipfile.ZipFile(self.file, "r") as zip_ref:
		    zip_ref.extractall(self.tempDir)

	def renameDbFiles(self):
		self.fileDir = os.path.join(self.tempDir, "com.flyersoft.moonreaderp")
		fileListFile = os.path.join(self.fileDir, "_names.list")
		with open(fileListFile, "r") as f:
			filenames = f.readlines()

		for (i, fn) in enumerate(filenames):
			if "mrbooks.db" in fn:
				oldname = str(i+1) + ".tag"
				newname = fn.split("/")[-1].replace("\n", "")
				os.rename(os.path.join(self.fileDir, oldname), os.path.join(self.fileDir, newname))

	def parseDates(self, dateString):
		lines = dateString.split("\n")

		for line in lines:

			if line == "":
				break

			parts = line.replace("@", "|").split("|")
			date = int(parts[0]) * 60 * 60 * 24
			time = int(parts[1])
			words = int(parts[2])

			yield {
				"dateString": datetime.datetime.fromtimestamp(date).strftime("%Y-%m-%d"),
				"time": time / 1000,
				"words": words,
			}


	def readDatabase(self):
		dbFile = os.path.join(self.fileDir, "mrbooks.db")
		if(self.debug): print("Reading database: '" + dbFile + "'")
		
		with sqlite3.connect(dbFile) as conn:
			c = conn.cursor()

			info = []
			info.extend(c.execute("SELECT filename, book, author FROM tmpbooks"))
			info.extend(c.execute("SELECT filename, book, author FROM books"))
			bookInfo = {}
			for b in info:
				bookInfo[b[0]] = { "title": b[1], "author": b[2] }

			for row in c.execute("SELECT filename, usedTime, readWords, dates FROM statistics"):
				yield {
					"filename": row[0].split("/")[-1],
					"usedTime": row[1],
					"readWords": row[2],
					"dates": list(self.parseDates(row[3])),
					"author": bookInfo[row[0]]["author"] if row[0] in bookInfo else None,
					"title": bookInfo[row[0]]["title"] if row[0] in bookInfo else None
				}

	def parse(self, outputFile=None):
		self.createTempDir()
		self.unzip()
		self.renameDbFiles()

		books = list(self.readDatabase())		

		# To make books read on the same day appear in the correct sequence (DB sequence),
		# reverse the list.
		books.reverse()

		if outputFile is not None:
			with open(outputFile, 'w') as outfile:
				json.dump(books, outfile, indent=4)

		if(self.debug): print("Wrote to file '" + outputFile + "'")

		self.removeTempDir()

		return books

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description='Unpacks a Moon+ backup file (.mrpro), and extracts reading statistics data from it.')
	parser.add_argument('-f', '--file', required=True, action='store', type=str, help='The file to parse.')
	parser.add_argument('-o', '--out', required=True, action='store', type=str, help='The JSON file to store the results in.')
	parser.add_argument('-d', '--debug', action='store_true', help='Print debug information')

	args = parser.parse_args()

	bp = BackupParser(args.file, args.debug)
	bp.parse(args.out)
# Book Statistics

This is a simple Python 3 script that parses backup files from the Android eReading app "Moon+ Reader". When using Moon+, you can easily make a backup of your data to for example Dropbox. You can then run this script with your Dropbox path as input, and it will extract various statistics from the backup files and present them as a static web page.

If you have multiple reading devices, the script will make sure to read backup files from all of them (provided they are located in the given input folder), and aggregate the statistics for you. 

If you have Calibre installed, you can also provide the script with the path of your Calibre library in order to fetch more meta data (including book covers).

Example: https://erlenddahl.net/etc/books

_Note: Unfortunately, it does not look good on tiny screens yet, and the mouseover popups are also hard to use on a touch device. Therefore, it is recommended to look at the statistics on an old fashioned computer for now._

## Work in progress

This script is still being developed. My future goals are:

* More statistics. What is interesting?
* Add statistics from other apps than Moon+. Which eReading apps have statistics that can be extracted?
* Make a simple web service so that people can generate their own visualization without having to handle a Python script?

## Usage

_You need to have Python 3 with the package `pillow` installed in order to run this script._

    python visualize.py -f "C:\backup\location" -o "C:\output\directory"

If you want raw JSON files to do some processing of your own, you can also run the `parseBackup.py` script on individual backup files to extract data.

    python parseBackup.py -f "C:\my-backup-file.mrpro" -o "C:\result.json"
    
## Covers

Cover images are stored within the data\output folder of the output directory. If you don't have a Calibre library, or any covers are missing, you can add them to this folder manually. Files must be stored in .jpg format, with the name "[author]---[title].jpg", and will then be automatically detected. For example: "peter watts---blindsight.jpg".

#### Example:
This example shows how to generate a visualization of backup files stored in `C:\Dropbox\Apps\Books\.Moon+\Backup`, using a Calibre library located at `C:\Dropbox\Calibre-bibliotek` for additional data. The visualization is written to the folder `C:\BookStats`.

    python visualize.py -f "C:\Dropbox\Apps\Books\.Moon+\Backup" -l "C:\Dropbox\Calibre-bibliotek" -o "C:\BookStats"
If you don't have a Calibre library, you can skip the -l parameter.

    python visualize.py -f "C:\Dropbox\Apps\Books\.Moon+\Backup" -o "C:\BookStats"
    
You can also use a .csv file with manually added books if you want to add books that you have not read using Moon+.

    python visualize.py -f "C:\Dropbox\Apps\Books\.Moon+\Backup" -o "C:\BookStats" -e "C:\extra_books.csv"
    
This CSV file must look like this (with as many rows as you'd like, of course):

    Title;Author;Cover;Read start;Read end
    State Of The Union;Brad Thor;state_of_the_union.jpg;2019-02-06;2019-02-25
    Redemption Ark;Alastair Reynolds;redemption_ark.jpg;2019-01-10;2019-02-05
    Past Tense;Lee Child;past_tense.jpg;2019-01-04;2019-01-10
    
The cover images must be stored in the same folder as the CSV file (or the path must be adjusted correspondingly).

## How it works

`parseBackup.py` uncompresses the Moon+ backup file, finds the relevant files inside it, and extracts data from the reading database. Data are stored as a JSON file.

`visualize.py`, which is the easiest way of using these scripts, runs `parseBackup.py` to extract JSON files, and then aggregates these files into a single list. It also adds any manually added books if a CSV file is provided.

After generating a single list of books, `visualize.py` will copy the `visualization` folder, which contains the static visualization web page, to the output directory, and write the statistics data to `[output]\data\books.js`, which will then be read when opening the web page.

The web page itself is a simple HTML/JavaScript page that is designed using Bootstrap, D3 and jQuery. It will read the exported book data from `data\books.js`, and present them in a nice way.